#!/bin/bash

cd /home/tonyk/codes/linux/
sudo cp arch/x86/boot/bzImage /boot/vmlinuz-linux-tony
sudo make modules_install -j8
sudo make headers_install INSTALL_HDR_PATH=/usr -j8
sudo mkinitcpio -p tony
