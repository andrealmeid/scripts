#!/bin/bash

cd ~/codes/linux/tools/testing/selftests/futex/functional
./futex2_wait | grep ok
./futex2_waitv | grep ok
./futex2_requeue | grep ok
./futex_wait_timeout | grep ok
./futex_wait_wouldblock | grep ok

cd ~/codes/linux/tools/perf
./perf bench -r 20 futex2 wake -t 1000 -s
./perf bench -r 20 futex2 wake -t 1000 -s -S
./perf bench -r 20 futex2 wake-parallel -t 1000 -s
./perf bench -r 20 futex2 wake-parallel -t 1000 -s -S
./perf bench -r 20 futex2 requeue -t 1000 -s
./perf bench -r 20 futex2 requeue -t 1000 -s -S
