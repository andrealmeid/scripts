#!/bin/bash

path=vm/
#path=./
#t=(8 80 800 8000)
t=(6 60 600 6000)

echo "Results for hashing stress (op/sec):"

for n in ${t[*]}
do
	result1=$(tail -n 1 "$path"futex_hash_"$n" | grep  '[0-9]*' -o | head -n1)
	result2=$(tail -n 1 "$path"futex2_hash_"$n" | grep  '[0-9]*' -o | head -n1)

	echo -ne "\t$n threads: futex1, $result1, futex2, $result2\n"
done

echo "Results for wake tests:"
for n in ${t[*]}
do
	result1=$(tail -n 1 "$path"futex_wake_"$n" | grep  '[0-9]*\.[0-9]*' -o | head -n1)
	result2=$(tail -n 1 "$path"futex2_wake_"$n" | grep  '[0-9]*\.[0-9]*' -o | head -n1)

	echo -ne "\t$n threads: futex1, $result1, futex2, $result2\n"
done

echo "Results for wake-parallel tests:"
for n in ${t[*]}
do
	result1=$(tail -n 1 "$path"futex_wake-parallel_"$n" | grep  '[0-9]*\.[0-9]*' -o | head -n1)
	result2=$(tail -n 1 "$path"futex2_wake-parallel_"$n" | grep  '[0-9]*\.[0-9]*' -o | head -n1)

	echo -ne "\t$n threads: futex1, $result1, futex2, $result2\n"
done
