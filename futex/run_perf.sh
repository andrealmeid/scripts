#!/bin/bash

RUNS=100

# for t in 8 80 800 8000
for t in 6 60 600 6000
do
	for api in futex2
	do
		for test_ in wake wake-parallel hash
		do
       	        	./perf bench -r $RUNS $api $test_ -t $t > results/vm/"$api"_"$test_"_"$t"
       	 		./perf bench -r $RUNS $api $test_ -S -t $t > results/vm/"$api"_"$test_"_"$t"_shared
		done
	done
done
