#!/bin/bash

img=$1
path=/home/tonyk/vms/mnt

sudo mount $img mnt
cd ~/codes/linux/
sudo make headers_install INSTALL_HDR_PATH=$path/usr
sudo make modules_install INSTALL_MOD_PATH=$path
sleep 1
sudo umount $path
