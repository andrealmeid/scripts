#!/bin/bash

# enable and start service
services="sshd dhcpcd"
systemctl enable $services
systemctl start $services

# place ssh public key
mkdir .ssh
cat id_rsa.pub > .ssh/authorized_keys


mkdir "/etc/systemd/system/serial-getty@.service.d"

echo "[Service]" > "/etc/systemd/system/serial-getty@.service.d/override.conf"
echo "ExecStart=" >> "/etc/systemd/system/serial-getty@.service.d/override.conf"
echo 'ExecStart=-/usr/bin/agetty --autologin root --noclear %I $TERM' >> "/etc/systemd/system/serial-getty@.service.d/override.conf"

# create a user
useradd -m user -G wheel
mkdir /home/user/.ssh
cat id_rsa.pub > /home/user/.ssh/authorized_keys
echo "fish" >> /home/user/.bashrc
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# fill the fstab
echo "shared_folder /home/user/codes 9p trans=virtio,msize=262144 0 0" >> /etc/fstab

reboot
